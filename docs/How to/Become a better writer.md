---
tags:
  - Writing
  - Tips
---
# Become a better writer

1. Read a lot and pay attention to how things are written.
2. Imitate the styles and phrasing you like.
3. Write in a journal.
4. Talk about the things you want to write, it can help you structure.
5. Try to engage your reader, try not to bore them!
6. Don't wait for inspiration, just start!!!
7. Accept that writing is hard for everyone.
8. Use most of your time to revise you first draft, and get that first draft out as fast as you can.
9. Cut ruthlessly, never become too attached to your words.
	1. This also is found in modelling, one of the biggest rules is to not fall in love with your model!
10. Find a good editor that can help.
	1. Preferably someone that is not into the subject matter. That makes it easier to spot inconsistencies and places where you fall too much into jargon.
11. Take risks, put something from yourself into your writing. Don't be afraid to break the rules, but do it with intent.