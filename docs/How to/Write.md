---
tags:
  - Writing
---

# Write

> [!note] Of course, the source of the course
> [Coursera: Writing in the Sciences](https://www.coursera.org/learn/sciwrite?)
## General
![[What makes good writing]]

![[What makes a good writer]]

![[Become a better writer]]

## First Principles
## Creeds to live by
![[Keep It Simple Stupid]]
![[Don't fall in love with your work]]

## Dealing with verbosity
![[Cut the clutter]]
![[Avoid repetition in writing]]

## Style
![[Nouns vs Verbs]]

# Quotes
![[Zinsser - On writing well]]

![[Pascal - On time it takes to edit]]
 
![[Jane J. Robinson - on using We or I in scientific writing]]
