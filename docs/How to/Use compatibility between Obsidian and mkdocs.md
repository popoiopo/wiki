---
title: Use compatibility Obsidian and mkdocs
tags:
  - Tutorial
---
# Markdown compatibility Obsidian and mkdocs

## Page information

This page is made to be an example and a reminder to myself of all the things that are possibile in this website/note taking. As I'm using [Obsidian](https://obsidian.md), [MkDocs](https://www.mkdocs.org), and the [Material template](https://squidfunk.github.io/mkdocs-material/), it is a bit of a search to see what plays nice. Therefore, though the rest of this page may seem like nonsense, it is a repository of things that work, things that don't work, and things that I still intend to find a fix for. If you want to see the page source [click here](https://gitlab.com/popoiopo/wiki/-/raw/main/wiki/Wiki/How%20to/Markdown%20compatibility%20Obsidian%20and%20mkdocs.md).

## Links to documentation

https://squidfunk.github.io/mkdocs-material/

https://help.obsidian.md/Obsidian/Index


### Things that don't seem to work (yet)
1. [x] Includes of recordings
	1. [x] Small hickup, can only include recordings in mp4 format, and actual local videos need to be resized properly (e.g., width:560, height:315)
2. [x] Figure resizing
3. [x] Text highlights ==like this== and ~~this~~
4. [x] Transclusions
5. [x] tooltip link previews
6. [x] Nice header
7. [ ] Excalidraw figures that are rendered through excalidraw instead of png
8. [ ] Comments
9. [ ] Backlinks

## Code

We can highlight code in two ways `#!python range()` and `range()`

```python linenums="1" title="Example code" hl_lines="1 4"
# Only if statement
newlist = [expression for item in iterable if condition == True]

# If else statement (1)
newlist = [expression if condition == True else other_condition for item in iterable]
```

1.  :see_no_evil: I don't know why, but I keep forgetting these..

### Tabbed code
=== "C"

    ``` c
    #include <stdio.h>

    int main(void) {
      printf("Hello world!\n");
      return 0;
    }
    ```

=== "C++"

    ``` c++
    #include <iostream>

    int main(void) {
      std::cout << "Hello world!" << std::endl;
      return 0;
    }
    ```
    
=== "Python"

    ``` python
    print("Hello world!")
    ```

## Headers

### Header 3

#### Header 4

##### Header 5

###### Header 6

## Annotating
So we can also do annotations in math like this $f(x) = \frac{e_i}{sin(x)}$ and:

$$
A=\begin{Bmatrix}
a_1 & a_2 & a_3 & a_4 \\
b_1 & b_2 & b_3 & b_4 \\
c_1 & c_2 & c_3 & c_4 
\end{Bmatrix}
$$

But we also have ==highlight==, ~~strikethrough~~, <u>underline</u>, _cursive_, **bold** and `CMD+SHFT+CTRL-f`.

_You **can** combine them_

## Linking
[[Do python list comprehensions]]

[[Use compatibility between Obsidian and mkdocs#Math]]

## Lists

- Item 1
- Item 2
  - Item 2a
  - Item 2b
	  - item 2ba
		  - 2baa

1. item 1
2. item 2
	1. item 2.1
	2. item 2.2
		1. 2.2.1
		2. 2.2.2

## Includes

![[test_image.png]]

Resizing only works by adding the following right next to the figure for mkdocs: 
> [!example]
> \!\[\[image.png|500]]{: loading=lazy width="500" }

The first 500 after \| is to resize in obsidian, the next addition between brackets is to resize in mkdocs.

![[test_image.png|500]]{: loading=lazy width="400" }

This is an include from another file. It does not seem to do any formatting.
![[Do python list comprehensions]]

## Tables

| Method      | Description                          |
| ----------- | ------------------------------------ |
| `GET`       | :material-check:     Fetch resource  |
| `PUT`       | :material-check-all: Update resource |
| `DELETE`    | :material-close:     Delete resource |

## Buttons

We can make some buttons:

[[index|Home!]]{ .md-button }

[[index|Go Home!]]{ .md-button .md-button--primary }

[[index|Send us home! :fontawesome-solid-paper-plane:]]{ .md-button }

## Embedding Recordings

So far, only iframes work. And no local files can easily be references. TODO: Fix a way to include recordings and local videos (workaround for videos might be to add them on youtube?).

### Iframe works in both obsidian as mkdocs
<iframe width="560" height="315" src="https://www.youtube.com/embed/qmk7yImJiCM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
### Webm
It support webm which is for some reason the default for obsidian recordings. As you can see, I stil have some practicing to do 😅

<video width="400" height="100" controls> <source src="../../Attachments/audio/sample-webm-harmonica.webm" type="video/webm"> </video>

---

### MP3
For audio, we can also use MP3 through html. The only problem is that by inserting the file in this way, it only seems to work for mkdocs (the browser) and not in obsidian.

![type:audio](Kalimba.mp3)

<audio controls> <source src="../../Attachments/audio/Kalimba.mp3" type="audio/mpeg"> </audio>

---
### MP4 
![type:video](sample-mp4-file.mp4)

---

### MOV
We can display mov files, as screencaptures on mac is automatically mov.
![type:video](sample-mov-file.mov)

## Block quotes
> Human beings face ever more complex and urgent problems, and their effectiveness in dealing with these problems is a matter that is critical to the stability and continued progress of society.
\- Doug Engelbart, 1961


## TODOs 

- [x] #tags, [links](), **formatting** supported 
- [x] list syntax required (any unordered or ordered list supported) 
- [x] this is a complete item 
- [?] this is also a complete item (works with every character but not for mkdocs) 
- [ ] this is an incomplete item 
- [ ] tasks can be clicked in Preview to be checked off

## Horizontal line
---
___
***

## Footnotes
Here's a simple footnote,[^1] and here's a longer one.[^bignote] 
[^1]: Meaningful!
[^bignote]: Here's one with multiple paragraphs and code. Indent paragraphs to include them in the footnote. `{ my code }` Add as many paragraphs as you like.


## Comments
Here is some inline comments: %%You can't see this text%% (Can't see it) Here is a block comment: %% It can span multiple lines %%

## Callouts

These are often also called admonitions.
> [!INFO] 
> Here's a callout block. 
>> It supports **markdown** and [[index|internal linking]].

> [!note]
>hallo dit is een ding
>Nog een ding!
>Wat doen we hier nog?! $e_i$ [[index]]
>> [!tip] Iets anders dan tip?
>> Het is vrij makkelijk te nesten
>>> [!warning]
>> > kijken hoe dit werkt
## Mermaid

```mermaid
sequenceDiagram
    Alice->>+John: Hello John, how are you?
    Alice->>+John: John, can you hear me?
    John-->>-Alice: Hi Alice, I can hear you!
    John-->>-Alice: I feel great!
```

`Lorem ipsum dolor sit amet`

:   Sed sagittis eleifend rutrum. Donec vitae suscipit est. Nullam tempus
    tellus non sem sollicitudin, quis rutrum leo facilisis.

`Cras arcu libero`

:   Aliquam metus eros, pretium sed nulla venenatis, faucibus auctor ex. Proin
    ut eros sed sapien ullamcorper consequat. Nunc ligula ante.

    Duis mollis est eget nibh volutpat, fermentum aliquet dui mollis.
    Nam vulputate tincidunt fringilla.
    Nullam dignissim ultrices urna non auctor.

## Math

$$
\operatorname{ker} f=\{g\in G:f(g)=e_{H}\}{\mbox{.}}
$$

The homomorphism $f$ is injective if and only if its kernel is only the 
singleton set $e_G$, because otherwise $\exists a,b\in G$ with $a\neq b$ such 
that $f(a)=f(b)$.

## Tooltips
If you hover over these, you'll get a pop-up message.

[Hover me](https://example.com "I'm a tooltip!")

:material-information-outline:{ title="Important information" }




