---
tags:
  - Writing
aliases:
  - The importance of editing
---
# Cut the clutter
> [!note]
> Limit your text just to the words that get the idea across
>> [!danger] Be ruthless!
>> This intimately ties in with [[Don't fall in love with your work]]. Be ruthless in editing and cut what you don't need. Try the sentence without the extra words to see what works better.

### Common sources of clutter to cut out
1. Dead weight words and phrases
	1. As it is well known (can be removed and implied by reference)
	2. As it has been shown
	3. It can be regarded that
	4. It should be emphasized that
2. Empty words and phrases
	1. Methodologic
	2. important
3. Long words or phrases can often be shorter
	1. E.g., muscular and cardiorespiratory performance == Fitness
4. Unnecessary jargon and acronyms
5. [[Avoid repetition in writing]]
6. [[Adverbs]] 
7. Negatives
	1. The word "not" can often be removed without issue by rephrasing a bit.
	2. Especially double negatives can be removes.
8. There are/there is, can often be removed:
	- There are several mechanisms that could explain the findings.
	- Several mechanisms could explain the findings.
9. Needless [[Prepositions]]
	1. The meeting happened on Monday.
	2. The meeting happened Monday.


