![[Pasted image 20220902082707.png]]



# Tips
> [!tip] Showcase good writing
> Tell it like a story in active voice

> [!tip] Start and end with main finding
> We found that

> [!tip] Dont travel too far from your data
> Focus on what your data do prove, not what you had hoped your data would prove

> [!tip] Focus on the limitations that matter, not generic limitations
> Try to address all major limitations that readers might have on your specific study. Anticipate what they will criticize.

> [!tip] Make sure the take-home message is clear and consistent

> [!tip] Verb tense
> **Past**, when referring to study details, results, analysis, and background research:
> 1. We <u>found</u> that
> 2. Subjects <u>may have experienced</u>
> 3. Miller et al. <u>found</u>
> 
> **Present**, when talking about what the data suggest:
> 1. The greater weight loss <u>suggests</u>
> 2. The explanation for this difference <u>is</u> not clear
> 3. Potential explanations <u>include</u>

### Key finding
1. **We found that** ==answer to most important question==
2. Then move on to question b

### Limitations and how they were addressed

### Conclusion - Restate answers to a and b 
1.  Big picture
2. Take home message



