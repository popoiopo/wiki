---
tags:
  - Python
  - Programming
---
# Do python list comprehensions

```python linenums="1" title="Example code"
# Only if statement
newlist = [expression for item in iterable if condition == True] # (1)

# If else statement
newlist = [expression if condition == True else other_condition for item in iterable]
```

1.  :see_no_evil: I don't know why, but I keep forgetting this..
