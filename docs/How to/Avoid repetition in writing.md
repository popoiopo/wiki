---
tags:
  - Writing
  - Tips
---
# Avoid repetition in writing

>[!tip] Duplicate meaning? Skip!
> Avoid [[Tautology]], i.e., when you say the same thing twice but phrased differently.
> Avoid [[Pleonasm]], i.e., when you enumerate a certain characteristic that is already implied.

> [!tip] Think before you thesaurus
> If you notice that you're reaching for a thesaurus to find a synonym to avoid repeating yourself, ask yourself if you really need that second instance at all.

