---
tags:
  - Tips
---
# Perform a safari hard refresh

> [!info] Answer
CMD+E to clear cash, followed by CMD+R to refresh page
