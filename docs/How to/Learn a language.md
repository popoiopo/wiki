## What to learn first
Use the [[Pareto Principle]] to your advantage. Meaning that you learn the words that you will use 80% of the time. These words will, according to the [[Pareto Principle]] be about 20% of the language. Therefore, one approach is to look for the 2000 most spoken words in a language, and learn that. You'd learn enough to be able to understand most communication. You can do this through [Anki](https://apps.ankiweb.net) for example.

## Cognates
Many languages share a common root. The idea is that, for example, words that end with "tion", will also do so (or at least with their other language counterpart) in the other language.

| English       | French        | Spanish      | Italian       |
| ------------- | ------------- | ------------ | ------------- |
| Action        | Action        | Acción       | Azione        |
| Nation        | Nation        | Nación       | Nazione       |
| Solution      | Solution      | Solución     | Soluzione     |
| Communication | Communication | Comunicación | Comunicazione |

To find these, you can google something like `[target language] english loan words`.

## Learning phrases relevant to your life
1. Label home items around the house.
2. Watch `[target language]` shows on netflix with english subtitles, and once your more acclimated, switch that up.
3. Try journalling in the `[target language]`.
4. Try to talk as much in the language as possible.
5. Focus on your goals and search for words that apply to those goals.

## Other tips
1. The main thing with just about everything I guess, is to use the idea of [[Compound Learning]].
2. Find yourself an accountability partner. Someone to talk to in `[target language]`, or at least someone who is also learning a language.
3. Attend language meetups, where you can find people proficient in the language.


