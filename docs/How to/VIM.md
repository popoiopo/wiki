# Sources
1. vimtutor

# General notions
  1. To repeat a motion prepend it with a number:   2w
  2. The format for a change command is:
        - operator   [number]   motion
        - where:
	        - operator - is what to do, such as  d for delete
	        - [number] - is an optional count to repeat the motion
	        - motion   - moves over the text to operate on, such as  w (word), e (end of word),  $ (end of the line), etc.


# Bindings
| Mode   | Type     | binding                | description                                                      |
| ------ | -------- | ---------------------- | ---------------------------------------------------------------- |
| Normal | Edit     | dw                     | Delete word                                                      |
|        | Edit     | d$                     | Delete until end of line                                         |
|        | Edit     | dd                     | Delete full line                                                 |
|        | Edit     | d0                     | Delete beginning line to current pos                             |
|        | Edit     | U                      | Undo all that has been done on line                              |
|        | Edit     | u                      | Undo                                                             |
|        | Edit     | CTRL-r                 | Redo                                                             |
|        | Motion   | h j k l                | Move left down up right                                          |
|        | Motion   | w                      | Move start of next word                                          |
|        | Motion   | e                      | Move end of current word                                         |
|        | Motion   | $                      | Move to end of line                                              |
|        | Motion   | %                      | Move to matching ), ], or }.                                     |
|        | Motion   | 0                      | Move to beginning of line                                        |
|        | Nav      | H                      | Move cursor to top of screen                                     |
|        | Nav      | M                      | Move cursor to middle of screen                                  |
|        | Nav      | L                      | Move cursor to bottom of screen                                  |
|        | Nav      | CTRL - d               | Move cursor down half a screen                                   |
|        | Nav      | CTRL - u               | Move cursor up half a screen                                     |
|        | Nav      | CTRL - f               | Move cursor forward one full screen                              |
|        | Nav      | CTRL - o               | Go to previous cursor location (repeat to go further)            |
|        | Nav      | CTRL - i               | Go to next location of cursor                                    |
|        | Nav      | CTRL - b               | Move cursor back one full screen                                 |
|        | Nav      | /                      | Input search string after and find it ((n)ext or N for previous) |
|        | Operator | d                      | Delete (something)                                               |
|        | View     | zt                     | Put cursor line to top of screen                                 |
|        | View     | zb                     | Put cursor line to bottom of screen                              |
|        | View     | zz                     | Center cursor to screen                                          |
|        | Operator | r                      | Replace character with following char                            |
|        | Operator | c                      | Change removes characters and puts you in edit                   |
|        | Nav      | ''                     | Go to last position before jump                                  |
|        | Info     | CTRL - G               | See where you are in file                                        |
|        | Edit     | :%s/old/new/gc         | Prompt to change every occurance of `old` to `new`               |
|        | Nav      | ? `phrase`             | Backward search for `phrase`                                     |
|        | Command  | :! `external command`  | Run any external command line command                            |
|        | Command  | :w `filename`          | Save currently opened file under name `filename`                 |
|        | Command  | :r   `filename`        | Read file into current location                                  |
|        | Command  | :r !`external command` | Read external command output into current loc (e.g., :r !ls)     |
|        |          |                        |                                                                  |



