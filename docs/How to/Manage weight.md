Hier wat punten die denk ik van belang zijn bij het managen van gewicht. Veel van deze dingen liggen natuurlijk een stuk meer genuanceerd en vereisen wellicht wat meer uitleg. Maar als vuistregels kun je hier denk ik al vrij ver mee komen. Mochten er dingen zijn waar je referenties van wilt, waar je meer van wilt weten, of waarvan je denkt dat dat niet klopt dan hoor ik dat natuurlijk graag :)

## Praktische tips

-   Zoek eten die je lekker én gezond vind

-   Probeer geleidelijk meer naar de gezonde dingen te gaan en minder ongezond
	-   Koop bijvoorbeeld geen ongezonde dingen meer bij de supermarkt. Wat je niet in huis hebt kan je ook niet eten.
	-   Als je wel gezonde dingen koopt die je ergens naartoe helpen, zorg dat je deze ook met plezier eet.
	-   Probeer nieuwe dingen, je weet nooit of je iets lekker vind als je het nooit geprobeerd hebt!

-   Vasten voor een bepaald aantal uren kan helpen met gewicht controle
	-   Na 12 uur niet eten gaat het lichaam in een staat wat ketose heet. Dit betekent dat het vet verbrand in plaats van suiker reserves voor energie. Een makkelijke manier om hier gebruik van te maken is door na 8 uur savonds niet te eten, en pas te eten bij de lunch om 12 uur. Het lichaam heeft dan dus 16 uur niet gegeten, en verbruikt dan dus vet voor de energie.
		-   Sommige mensen hebben meer last van niet eten dan andere. Het is ook niet voor iedereen, maar om het een goede kans van slagen te geven is het misschien een goed idee om elke (of week) een uurtje later ontbijt te hebben. Wacht als je honger hebt een half uurtje met eten, en kijk of je dan nog steeds honger hebt. Als de honger voorbij is gegaan kan je verder zoals geplanned, en anders kun je alsnog gewoon wat eten.
	-   Verder werkt dit soort vasten doordat je simpelweg minder tijd hebt om de calorieën tot je te nemen.
	-   Als dit een te lange tijd zonder eten is, kan je proberen je ontbijt steeds met bijvoorbeeld een uurtje te verschuiven. Zo verklein je de tijd waarin je eet.
	-   Let wel op dat je genoeg nodige voedingsstoffen binnen krijgt. En probeer het niet te forceren. Dit is niet voor iedereen, als het lukt en het bevalt dan is dat top, maar als het niet lukt nadat je het een goede kans hebt gegeven, dan is dat ook niet zo erg.
	-   Vasten heeft niet alleen voordelen voor gewichtscontrole.
		-   Er is wat bewijs voor dat het slechte cholesterol gehalte naar beneden gaat.
		-   De bloeddruk wordt verlaagd.
		-   Insuline resistentie word verlaagd
		-   reducing the risk for obesity, diabetes, and cardiovascular diseases.

## Gebruik van gewoontes

-   Gezonde gewoontes opbouwen is belangrijk voor het behouden van een "groei mindset". Elke dag een klein beetje beter, is beter dan een keer beter en dan nooit meer. Zelf denk ik graag aan babysteps! Elke dag een klein stapje, hoe onbenullig dat voor andere mensen ook lijkt, zolang je vooruit gaat kom je er wel!
	-   Denk bijvoorbeeld aan schoonmaken in rommelig huis. Als je elke dag iets meer op ruimt maakt dan dat je rommelig maakt, dan hoef je uiteindelijk alleen maar hetgeen op te ruimen wat je die dag rommelig hebt gemaakt. Alleen met groei en gezondheid altijd doorgroeien, wat iets goeds is!
	-   Als je elke dag 2 oefeningen doet voor drie sets (totaal iets van 6 minuutjes werk), dan zal je daar veel meer verbetering in zien dan een keer in de twee weken anderhalf uur lang sporten. In totaal kost dit ongeveer even veel tijd, maar elke dag een beetje zorgt voor veel meer vooruitgang.
	-   Een slechte dag, of week, betekent niet dat je klaar bent met je doelen bereiken of dat je het maar op moet geven. Wees niet te streng voor jezelf, en begin gewoon weer, zelfs al is dat een paar stapjes terug. Baby steps!

-   Je kunt bijvoorbeeld een paar dingen die je leuk vind mixen met iets wat wellicht wat ongezond is en je misschien wat minder leuk vind maar wel gezond zijn.
	-   Tv kijken terwijl je yoga/rek en strek oefeningen doet.
	-   Rondlopen terwijl je aan de telefoon zit (of op de loopband tijdens het werk in mijn geval)

-   Zorg dat je omgeving in lijn is met je doelen
	-   Als je ongezonde dingen in de buurt hebt die je niet helpen je doelen te bereiken, maar je die wel teveel doet als ze in de buurt zijn. Probeer deze dan uit de weg te ruimen. Verhoog de grens voor jezelf zo veel mogelijk om er mee te beginnen.
		-   Bijvoorbeeld bij teveel telefoon of tablet gebruik. Gebruik een app die het vervelend maakt om bepaalde apps te openen, of zet de kleuren op zwart wit (dit helpt je scherm wat minder interessant te maken, en geeft je brein meer rust).
	-   Andersom werkt het ook. Als je iets wat je meer wilt doen en wat je blijer, flexibeler, sterker maakt, zorg dat dat makkelijk bereikbaar is. Verlaag de grens voor jezelf zoveel mogelijk om er mee te beginnen.
	-   In het kader van "zorg dat ongezonde dingen moeilijk verkrijgbaar zijn", ik koop bijvoorbeeld geen vlees, snoep, koek en ijs meer in de supermarkt. Zo nu en dan eet ik deze dingen nog wel, maar ik zorg er voor dat ik ze niet altijd meer in huis heb. Op deze manier ben ik veel bewuster in wat ik eet/doe, in plaats van pakken wat toevallig in de buurt is.

## Het bijhouden en managen van gewicht

-   Sommige mensen vinden het geen probleem om calorieën bij te houden. In dit geval bereken je met enige regelmaat opnieuw wat je benodigde calorieën zijn en hou je daar aan.
	-   Als je wilt afvallen zorg je dat je 20% minder calorieën binnen krijgt dan die op maintenance calories:
		- Gebruik bijvoorbeeld een website om je maintenance calorieën te berekenen (e.g., https://www.calculator.net/calorie-calculator.html).

-   Het gaat er om dat je inzicht krijgt in je voedingspatroon. Wat eet je precies en hoeveel calorieën krijg je hiermee binnen? Wat zijn verbeterpunten?
	-   Wanneer snoep je bijvoorbeeld vaak, waarom, en kan je dit voorkomen?
	-   Drink je ongezond? Een hoop calorieën komen via dranken.
	-   Let ook wel op, artificial sweeteners kunnen ook negatieve gezondheids gevolgen hebben. Het kan bij sommige namelijk bijvoorbeeld zijn dat je trek in zoete dingen niet weg gaat door vervangers, waardoor je alsnog meer zoete dingen gaat eten.
		- Sucralose activeerd bijvoorbeeld je smaak beloningssysteem in de hersenen, maar zorgt er niet voor dat je je niet voldaan voelt m.b.t. de calorieën die je tot je neemt. Hierdoor heb je nog steeds trek, en zal je wellicht meer eten dan je anders zou doen.   
	-   Weet wat je eet.
		-   Dit kan op meer manieren, sommige mensen houden een eet dagboek bij, en zetten daar alles in. Maar sommige mensen ervaren dit juist weer als een grote last, en vinden de administratie zodanig belastend dat ze het hele doel maar opgeven. Probeer te vinden wat voor jou werkt.

-   Zelf hou ik niet zo van de administratie van calorieën bijhouden en doe ik dit meer door mij elke dag te wegen, en te kijken naar wat de week gemiddeldes zijn. Is het doel om af te vallen en gaat dit een beetje naar beneden, dan is dat prima. Gaat het omhoog, dan kijk ik wat ik die week heb gedaan/gegeten, en pas ik dat voor de volgende keer aan. Hiermee probeer ik meer bewust te zijn in de keuzes die ik maak.
	- Ik doe bijvoorbeeld aan intermittent fasting en sla ontbijt over. Dit voelt voor mij als een erg natuurlijke methode omdat ik eten in de ochtend toch niet fijn vind.
	- Heel saai, maar ik eet bijvoorbeeld altijd vrijwel dezelfde lunch, met hier en daar iets extraas. 
	- En voor avond eten eet ik vaak vega/vegan. Zo weet ik ongeveer wat ik binnen krijg, wat voor mij voldoende werkt. Ik zou wel aanraden om een tijdje goed onder de loep te nemen wat je nou precies eet en drinkt, en kijk of dit je helpt. Dit alles lijkt voor mij goed te werken, kijk wat werkt voor jou.

-   Let wel op dat het wegen van gewicht per dag erg kan verschillen. Als je een dag opeens anderhalve kilo zwaarder weegt dan een voorgaande dag is dit niet perse erg. Dit kan meerdere redenen hebben namelijk. Het is dan beter om naar bijvoorbeeld week gemiddeldes te kijken.
	-   Zo kan je bijvoorbeeld veel zout hebben gegeten de dag ervoor, en daardoor veel vocht vast houden.
	-   De tijd waarop je weegt is enorm van belang. Mensen zijn in de ochtend (en na een toilet bezoek) bijvoorbeeld lichter dan direct na het avond eten. Dit is omdat je best veel vocht verliest in de nacht.
	-   Heb je gesport, veel gegeten, welke tijd meet je jezelf, dat zijn allemaal dingen die mee spelen. Dus kort gezegd, let niet te veel op dag tot dag schommeling.

## Hoe kan je naar gewichtsmanagement kijken?

-  Als het lichaam krijgt meer energie (calorieën) krijgt dan het gebruikt dan wordt het opgeslagen als vet.
	-   Op het moment dat je afvalt, verbruikt je lichaam ook minder energie omdat het simpelweg kleiner is. Hierdoor val je dus wat minder snel af als je voedsel intake hetzelfde blijft. Houd je verwachting dus realistisch.
	-   Een goed weetje is dat afvallen niet werkt op specifieke plekken. Het gebeurt gelijkmatig hetzelfde over het hele lichaam. Buikspier oefeningen voor een kleine taille werkt dus bijvoorbeeld niet. Probeer daarvoor oefeningen die meer energie verbranden, zoals het gebruik van grote spiergroepen. Door bijvoorbeeld benen en rug te trainen gebruik je meer energie aangezien dit grote spieren zijn die veel energie verbruiken.

-   Doe dingen langzaam en op je eigen tempo. Het is beter om langzaam over lange tijd af te vallen, dan opeens radicaal je leven te veranderen. Kijk naar lange termijn verbetering. Verbetering dat dus komt door elke dag een klein beetje beter te zijn, in plaats van in een maand alles te verbeteren. Je eigen tempo, met dingen die je lekker vind, is veel houdbaarder dan een dieet volgen dat iemand anders heeft bedacht. Hiervoor moet je alleen wel een actieve en onderzoekende houding hebben om te kijken wat je nou inderdaad lekker vind wat ook gezond is.

-   Wat is de reden waarom je gewicht wilt verliezen? Motivatie die vanuit jezelf komt (dit heet intrinsieke motivatie) is veel effectiever dan motivatie die door andere mensen opgelegd wordt (extrinsieke motivatie). Denk bijvoorbeeld aan dat iemand die een muziek instrument leert spelen veel sneller en makkelijker zal leren wanneer deze dat leuk vind en zelf belangrijk, dan iemand die het leert omdat het moet van school of ouders. Voorbeelden van motivatie zijn bijvoorbeeld:
	-   Er goed uit zien
	-   Gezond zijn
		-   Energiek zijn
		-   Goed en zonder pijn kunnen bewegen op de manier dat je zou willen.
			-   Mobiliteit en flexibiliteit maken hierbij bijvoorbeeld een groot verschil. Rek en strek oefening in combinatie met wat kracht oefeningen kunnen je al erg ver brengen.
		-   Langer in je leven vitaal zijn
			-   Er wordt vaak onderscheid gemaakt in gezonde levensjaren en absolute levensjaren. Iemand kan bijvoorbeeld 90 worden, maar op hun 60ste al gezondheidsproblemen krijgen. Deze persoon heeft dan 30 jaar moeite met bepaalde dingen waardoor de kwaliteit van leven omlaag gaat. Het aantal gezonde levensjaren zou dan dus 60 jaar zijn terwijl de absolute leeftijd 90 is. Het doel is dan eigenlijk om dit verschil zo klein mogelijk te maken, waardoor je zo lang mogelijk de vrijheid behoud om de dingen te doen die je gelukkig maakt.
	-   Gewichtsverlies puur als doel.
		-   In dit geval denk ik dat het beter is om verder dan dat te kijken. Zoek dieper en kijk waarom gewichtsverlies een doel is. Gewichtsverlies kan beter een gelukkige bijkomstigheid zijn, dan een doel op zich. Dit heeft vooral met houdbaarheid te maken. Als je je streefgewicht behaald, dan "kan je stoppen" omdat je "klaar" bent. En dit is wanneer het risico weer terug komt om het gewicht terug te krijgen. Vaak is dit ook meer een extrinsieke motivatie (dus een zwakkere motivatie van buitenaf), dan dat het een intrinsieke motivator is die echt vanuit jezelf komt.
	-   Zodra je weet wat je drijfveer is, dan kan je gaan kijken naar wat je kan helpen om je in die richting te krijgen

-   Het lichaam is een complex systeem. Er is een hele hoop verbonden met elkaar, dus focussen op een enkel ding (zoals sport bij gewichtsverlies), is misschien niet de beste route. Kijk naar het totaal plaatje. Probeer fysiek actief te zijn, maar kijk ook naar eten, en kijk bijvoorbeeld ook naar wat je gelukkig maakt. Geluk heeft ook te maken met hoe energiek je bent, wat weer te maken heeft met hoe veel je zou willen sporten, of hoe veel energie je hebt om iets leuks te doen, of gezond te koken. Deze dingen hebben allemaal met elkaar te maken.

-   Compound interest. Langdurig elke dag 1% beter zijn is iets waar je veel meer aan hebt dan eenmalig heel veel doen en daarna het opgeven.
	-   Consistency is key

-   Het gaat niet zo zeer om afvallen, maar meer om een gezondere leefstijl te krijgen dat afvallen als bijproduct heeft. Als je namelijk een hoop af valt door een tijdelijke verandering, maar daarna weer terug valt in het normale patroon, dan past je lichaam zich weer aan aan dat patroon. Wat betekent dat er weer gewicht terug komt.
	-   Langdurig eenzelfde gewicht aanhouden die lager is dan je begingewicht is ook al een overwinning. Dat betekent namelijk dat je een leefstijl hebt die goed werkt met dit lagere gewicht.