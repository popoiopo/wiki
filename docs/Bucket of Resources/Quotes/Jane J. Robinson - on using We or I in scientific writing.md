> [!quote]
> After all, human agents are responsible for designing experiments, and they are present in the laboratory; writing awkward phrases to avoid admitting their responsibility and their presence is an odd way of being objective.
> 
> \- Jane J. Robinson, Science 7 June 1957: 1160

