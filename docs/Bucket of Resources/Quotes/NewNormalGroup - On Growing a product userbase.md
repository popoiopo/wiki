> [!quote] Get 100 loyal customers
> When working with a new product line, which is typical at most startups, it is more important in those early stages to build a base of 100 loyal customers that genuinely love your product, instead of 1000 mediocre customers that don't really understand or value your product.
> 
> As we went about making our first enthusiastic (and demanding) customers happy, it was those original "100" loyal customers that started recommending us to their friends, and there began our organically growing customer acquisition.
> 
> \- [newnormalgroup.com](https://newnormalgroup.com/stories/business-insight/why-organic-customer-acquisition-is-key-for-exponential-growth)

> [!quote] Search Engine Optimization
> To start with, you need to know technology (or have someone who knows technology help you). Google, the no. 1 search engine in the world, does discriminate on the technical quality of your website. If you have a slow site or if your site has other technical issues, you will never get a high ranking in Google, no matter how much money you spend on SEO-related help. 
> 
> Secondly, content is king. Quality content, which delivers real value and understands and solves the issues from reader queries, always builds the basis of an organic search strategy. If done well, your content will help you connect and build trust with your customer base. 
> 
> To round off the topic of SEO, you should be prepared to fine-tune details when it comes to optimization. One of them includes linking to metadata, which will get you great rankings and help drive organic traffic when combined with the right content-filled, high-quality website.
> 
> \- [newnormalgroup.com](https://newnormalgroup.com/stories/business-insight/why-organic-customer-acquisition-is-key-for-exponential-growth)

