> [!quote] Blaise Pascal, _Lettres provinciales_ - 1656
> I have only made this letter rather long because I have not had time to make it shorter.
> 
> _Also attributed to St. Augustine and Cicero_

