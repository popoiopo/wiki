" Have j and k navigate visual lines rather than logical ones
nmap j gj
nmap k gk

" Map k j to escape so that i don't have to reach for the furthest key 
imap kj <Esc>

" Yank to system clipboard
set clipboard=unnamed
