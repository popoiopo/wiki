---
tags:
  - Writing
  - Tips
---
# What makes good writing?

1. It needs to communicate an idea clearly and effectively
	2. This just takes having something to say, and clear thinking.
2. Good writing is elegant and stylish
	1. This is much less important though, focus on point number one!
	2. This happens during the editing phase!
