---
tags:
  - Writing
  - Tips
---
# What makes a good writer

You can learn to be a good writer, all you need is:
1. Have something to say.
2. Logical thinking.
	1. Structure your story well and in a well thought out manner.
3. Know the few simple and learnable rules of style.