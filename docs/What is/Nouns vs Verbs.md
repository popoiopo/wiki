---
tags:
  - Tips
  - Writing
aliases:
  - "Keep it active"
---
# Nouns vs. Verbs

> [!tip] 
> Try to avoid nouns (like production, recycling, activation) and use verbs (to produce, recycle, activate) instead.
> 
> Nouns tend to slow the sentence down, so keep it active.


## What is the active voice?
There is an agent and a recipient in a sentence. One way to identify an active voice, is to see if the agent comes before the recipient:

**She** (agent) throws **the ball** (recipient).

or:

**I** (agent) will always **remember** (verb) **my first visit to mexico** (recipient).

### Why use active voice
The active voice forces sentences to be more direct, and helps cutting out unnecessary words in the process. So it:

1. Emphasizes author responsibility
2. Improves readability
3. Reduces ambiguity

## What is a passive voice?
If the agent and recipient trade places, the sentence becomes passive:

**The ball** is thrown by **her**.

The passive voice always has a "to be" verb (could be, shall be, should, will, would, may, might, must, has been) + a past particle of the main verb.

**My first visit to Mexico** (recipient) will always **be remembered** (verb) by **me** (agent).

### Why use the passive voice?
1. When you want to create mystery (as the agent is omitted).
2. When you want to avoid describing a responsible party.
3. When what is done, is more important than who did it.

### Why to avoid passive voice?
1. It is not how we talk in normal conversation.
2. It is a way to avoid responsibility:
	1. The sentence: "Mistakes were made." avoids to use an agent, and therefore makes is seem as if something passively happened without anyone to account for the action.