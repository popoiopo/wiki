> [!tip]
> The idea of compound learning is to do a small amount of practise every day, without fail. 
> 
> Compound learning is **the knowledge equivalent of compound interest**. If compound interest can be viewed as “interest-on-interest,” then compound learning can be viewed as “knowledge-on-knowledge,” where both can make an amount grow at a faster rate. 
> 
> \- _[source](https://blogs.gartner.com/power-of-the-profession-blog/warehouse-future-power-compound-learning-robotics/)_



