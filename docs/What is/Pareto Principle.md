> [!note]
> The **Pareto principle** states that for many outcomes, roughly 80% of consequences come from 20% of causes (the "vital few"). Other names for this principle are the **80/20 rule**, the **law of the vital few,** or the **principle of factor sparsity.**

