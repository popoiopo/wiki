---
tags:
  - Writing
---
# Pleonasm

> [!note] Definition
> A pleonasm arises when you describe a property of an object while it is already implied; the white snow, yellow sunflowers, or the oral discussion.

