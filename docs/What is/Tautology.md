---
tags:
  - Writing
---
# Tautology

> [!note] What is it?
> A tautology arises when you say the same thing twice but with different words, for example: happy and pleased or presumably and probably. **Note**: it happens that a tautology is sometimes used in a good way. This is then done to strengthen the expressiveness of a sentence.

