---
tags:
  - Writing
  - Grammar
---

# Prepositions

> [!note] Definition
> Prepositions tell us where or when something is in relation to something else. When monsters are approaching, it’s good to have these special words to tell us where those monsters are. Are they **behind** us or **in front of** us? Will they be arriving **in** three seconds or **at** midnight?
> 
> [See grammarly article.](https://www.grammarly.com/blog/prepositions/)

