> [!note] 
> A veridical paradox is a paradox that is provably true, but feels false. It therefore says more about psychology than about logic.
