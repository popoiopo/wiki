---
tags:
  - Language
  - Grammar
---
# Adverbs

> [!note] Definition
> An adverb is a word that modifies (describes) a [[Verb]] (he sings loudly), an [[Adjective]] (very tall), another adverb (ended too quickly), or even a whole sentence (Fortunately, I had brought an umbrella). Adverbs often end in -ly, but some (such as fast) look exactly the same as their adjective counterparts.