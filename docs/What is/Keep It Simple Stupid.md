---
tags:
  - Tips
---
# KISS (Keep It Simple Stupid)
> [!note] The general idea
> Complex ideas don't require a complex solution (be it language in writing or architecture in software development).

Like the title said, don't overengineer or overcomplicate things. Keep it as simple and as easy to understand as possible.

> [!tip]
> Avoid unnecessary structures (like jargon, acronyms, and classes). Things are usually complicated, so think about whether you need everything you're using.
> > [!tip] Tipception
> > It's often easier to remove than to write concisely immediately. Don't be afraid to delete something, yet don't focus too much on avoiding verbosity either; you can iron things out later.

