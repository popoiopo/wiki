---
tags:
  - Writing
---
# Contamination in language

> [!note] What is it?
> Contamination occurs when two words or phrases are mixed together. These are also referred to as cocktail words or phrases.