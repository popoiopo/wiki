---
tags:
  - Tips
---
# Don't fall in love with your work

> [!warning]
> Don't fall in love with your work as you might need to change it! 

When making a computational model or writing a paper, you often try something that doesn't work. Try not to become too attached to what you produced as editing, removing bits and pieces, will hurt! Keep yourself from caring too much, causing you to stand in your own way to improve whatever you're working on.