---
index: true
template: index.html
hide:
  - navigation
  - toc
---

This is the start, the root of the tree. Here we branch out to the who, the what, and the how. We'll start by formulating questions or statements that go deeper and deeper:

<div class="grid cards" markdown>

> [!tip] [[0. How to]] 👷‍♀️
> How to? These are all the things that relate to practical matters and how they are done. Think of it as a repository of tutorials.

> [!example] [[1. I made]] 🦾
> I made! Some things that I made and make publicly available 😁

</div>

<div class="grid cards" markdown>
> [!abstract] [[2. What is]] 🧠
> What is? Things of a more theoretical nature regarding concepts, and their relationship between one another. Think of it as a personal wikipedia.

> [!question] [[3. Who is]] 💃
> Who is? This concerns information about people, what they've done and are known for. Don't worry, this is not used for people I personally know 😉
</div>

