site_name: Bas Châtel's Corner of the Interwebs
site_description: My repository of notes, blogposts, and all other stuff I'd want to share.
site_url: https://popoiopo.gitlab.io/wiki
site_dir: public
docs_dir: ./docs

theme:
    name: 'material'
    logo: assets/meta/icon.png
    favicon: assets/meta/favicon.ico
    custom_dir: overrides
    font:
        text: Ubuntu
        code: Ubuntu Mono
    language: en
    palette:

    # Light mode
    - media: "(prefers-color-scheme: light)"
      scheme: default
      primary: teal
      accent: light blue
      toggle:
        icon: material/weather-sunny
        name: Switch to dark mode

    # Dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      primary: indigo
      accent: cyan
      toggle:
        icon: material/weather-night
        name: Switch to light mode
    features:
        - navigation.indexes
        - navigation.top
        - navigation.tabs
        - navigation.tabs.sticky
        - navigation.expand
        - search.suggest
        - search.highlight
        - content.code.annotate
        - content.tabs.link

# Extensions
markdown_extensions:
  - footnotes
  - nl2br
  - attr_list
  - sane_lists
  - md_in_html
  - meta
  - smarty
  - tables
  - mdx_breakless_lists
  - def_list
  - pymdownx.arithmatex:
      generic: true
  - pymdownx.details
  - pymdownx.magiclink
  - pymdownx.critic
  - pymdownx.caret
  - pymdownx.keys
  - pymdownx.mark
  - pymdownx.tilde
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.highlight:
      use_pygments: true
      anchor_linenums: true
  - pymdownx.tasklist:
       custom_checkbox: true
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
  - admonition
  - toc:
      permalink: true
  - pymdownx.inlinehilite
  - pymdownx.snippets
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:mermaid2.fence_mermaid
plugins:
  - search
  - ezlinks:
        wikilinks: true
  - mermaid2
  - awesome-pages
  - tags:
      tags_file: tags.md
  - tooltipster-links:
      callouts: true
      custom-attributes: 'assets/css/custom_attributes.css'
      max-characters: 500
      truncate-character: '...'
  - embed_file:
      callouts: true
      custom-attributes: 'assets/css/custom_attributes.css'
  - callouts
  # - mkdocs-simple-hooks:
  #     hooks:
  #       on_env: "overrides.hooks.on_env:on_env"
  #       on_page_markdown: "overrides.hooks.on_page_markdown:on_page_markdown"
  - encryptcontent:
      title_prefix: '🔐'
      password_button: True
      password_button_text: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="1em" width="1em"><!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M416 32h-64c-17.67 0-32 14.33-32 32s14.33 32 32 32h64c17.67 0 32 14.33 32 32v256c0 17.67-14.33 32-32 32h-64c-17.67 0-32 14.33-32 32s14.33 32 32 32h64c53.02 0 96-42.98 96-96V128C512 74.98 469 32 416 32zM342.6 233.4l-128-128c-12.51-12.51-32.76-12.49-45.25 0c-12.5 12.5-12.5 32.75 0 45.25L242.8 224H32C14.31 224 0 238.3 0 256s14.31 32 32 32h210.8l-73.38 73.38c-12.5 12.5-12.5 32.75 0 45.25s32.75 12.5 45.25 0l128-128C355.1 266.1 355.1 245.9 342.6 233.4z"/></svg>'
  - custom-attributes:
      file: 'assets/css/custom_attributes.css'
  - exclude:
      glob:
        - Templates/*
        - Excalidraw/*
  - mkdocs-video:
      is_video: True

extra_javascript:
  - assets/js/mathjax.js
  - assets/js/utils.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
  - assets/js/tooltipster.bundle.js
  - https://unpkg.com/tablesort@5.3.0/dist/tablesort.min.js
  - assets/js/tablesort.js
  - https://www.googletagmanager.com/gtag/js?id=G-B6PQE607LS"
  - assets/js/google_analytics.js
extra_css:
    - https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css
    - assets/css/template/blog.css
    - assets/css/template/grid.css
    - assets/css/template/img-grids-floats.css
    - assets/css/template/tooltipster.bundle.min.css
    - assets/css/template/utils.css
    - assets/css/admonition.css
    - assets/css/custom_attributes.css
    - assets/css/customization.css
extra:
  generator: false
  SEO: assets/meta/bc_icon-256x256.png
  blog_list:
    pagination: true
    pagination_message: true
    pagination_translation: 'posts in'
  hooks:
    strip_comments: true
    fix_heading: true
  analytics:
    provider: google
    property: G-B6PQE607LS
  feedback:
      title: Was this page helpful?
      ratings:
        - icon: material/emoticon-happy-outline
          name: This page was helpful
          data: 1
          note:
            Thanks for your feedback!
        - icon: material/emoticon-sad-outline
          name: This page could be improved
          data: 0
          note:
            Thanks for your feedback! Help me improve this page by
            using the <a href="https://docs.google.com/forms/d/e/1FAIpQLSc0C8BHAGmOayzl130CDzZJRzkABLU-XQ3VtTPlbK960VzmFw/viewform?usp=sf_link" target="_blank" rel="noopener">feedback form</a>.
